(ns app.queries
  (:require
    [datomic.api :as d]
    [app.db :as db]))


;;ADD

(defn list-id [db]
  (ffirst (d/q '[:find ?list-id :where [?list-id :todo-list/id]] db)))

(defn new-id [db]
  (let [query '[:find ?item-id :where [_ :todo-item/id ?item-id]]
        current-id (count (d/q query db))
        new-id (inc current-id)] new-id))

(defn transact-todo [db value]
  (let [db (d/db db)
        list-id (list-id db)
        new-id (new-id db)]
    @(d/transact @db/conn [{:db/id (d/tempid :db.part/user)
                            :todo-item/id new-id :todo-item/value value}
                           [:db/add list-id :todo-list/items new-id]])))


;;Edit - Delete

(defn get-ident [db id]
  (ffirst (d/q '[:find ?e
                 :in $ ?id
                 :where
                 [?e :todo-item/id ?id]] db id)))

(defn item-count [db]
  (let [query '[:find ?item-id
                :where [_ :todo-item/id ?item-id]]
        current-id (count (d/q query db))] current-id))



;;Resolvers

(defn item-by-id [db id]
  (d/pull (d/db db) '[:db/id :todo-item/id :todo-item/value] [:todo-item/id id]))


(defn list-items-by-id [db]
  (let [db (d/db db)
        query '[:find (pull ?e pattern)
                :in $ pattern
                :where [?e :todo-item/id _]]
        result (d/q query db '[:todo-item/id])]
    (mapv first result)))

(defn count-items [db]
  (let [db (d/db db)
        query '[:find (count ?id)
                :where [:todo-item/id ?id]]
        result (d/q query db)]
    (mapv first result)))