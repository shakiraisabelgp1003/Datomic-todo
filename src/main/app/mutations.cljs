(ns app.mutations
  (:require
  [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]
  [com.fulcrologic.fulcro.algorithms.normalized-state :as fns]))

(defmutation add-todo
  [{:todo-item/keys [value]}]
  (action [{:keys [state]}]
          (let [new-id (inc (get-in @state [:todo-list :todo-list/item-count]))
                new-item {:todo-item/id new-id :todo-item/value value}]
        (fns/swap!-> state
            (update-in [:todo-item/id] assoc new-id new-item)
            (update-in [:todo-list :todo-list/items] conj [:todo-item/id new-id])
            (update-in [:todo-list :todo-list/item-count] inc))))
  (remote [env] true))


(defmutation edit-todo [{:keys [id new-value]}]
  (action [{:keys [state]}]
          (swap! state assoc-in [:todo-item/id id :todo-item/value] new-value))
  (remote [env] true))

(defmutation delete-todo [{:todo-item/keys [id]}]
             (action [{:keys [state]}]
                  (fns/swap!-> state
                    (update-in [:todo-list :todo-list/items]
                    (fn [items] (vec (remove #(= (second %) id) items))))))
             (remote [env] true))