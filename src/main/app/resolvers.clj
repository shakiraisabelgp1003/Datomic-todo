(ns app.resolvers
  (:require
    [com.wsscode.pathom.core :as p]
    [com.wsscode.pathom.connect :as pc]
    [datomic.api :as d]
    [app.queries :as q]
    [app.db :as db]
  ))

(pc/defresolver todo-item-by-id [env {:todo-item/keys [id]}]
                {::pc/input  #{:todo-item/id}
                 ::pc/output [:todo-item/id :todo-item/value]}
                (q/item-by-id @db/conn id))


(pc/defresolver todo-list-resolver [env {:todo-list/keys [id]}]
                {::pc/output [:todo-list/id {:todo-list/items [:todo-item/id]}]}
                (let [pattern (::core/parent-query env)]
                (println pattern))
                {:todo-list/id id
                 :todo-list/items (q/list-items-by-id  @db/conn)}))

(pc/defresolver todo-list-item-count [env {:todo-list/keys [id]}]
                {::pc/input  #{:todo-list/id}
                 ::pc/output [:todo-list/item-count]}
                 {:todo-list/item-count (q/count-items @db/conn)})


(pc/defresolver root-resolver [env params]
                {::pc/output [{:todo-list [:todo-list/items]}]}
                {:todo-list {:todo-list/id 1}})

(def resolvers [todo-list-resolver todo-item-by-id root-resolver])