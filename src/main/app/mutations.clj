(ns app.mutations
  (:require
    [app.db :as db]
    [app.queries :as q]
    [datomic.api :as d]
    [com.wsscode.pathom.connect :as pc]))

    
(pc/defmutation add-todo [env {:todo-item/keys [value]}]
                {::pc/sym `add-todo}
                (q/transact-todo @db/conn value))

(pc/defmutation edit-todo [env {id :todo-item/id new-value :todo-item/value}]
                {::pc/sym `edit-todo}
                (let [db (d/db @db/conn)
                      ident (q/get-ident db id)]
                  @(d/transact @db/conn [{:db/id ident :todo-item/value new-value}])))

(pc/defmutation delete-todo [env {:todo-item/keys [id]}]
                {::pc/sym `delete-todo}
                (let [db (d/db @db/conn)
                      ident (q/get-ident db id)]
                  @(d/transact @db/conn [[:db/retractEntity ident]])))

(def mutations [add-todo edit-todo delete-todo])